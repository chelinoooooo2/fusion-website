// Convert RGB or RGBA to Hex
const rgbToHex = (rgb) => {
    const result = rgb.match(/\d+/g);
    return result
      ? `#${(
          1 << 24 |
          (parseInt(result[0]) << 16) |
          (parseInt(result[1]) << 8) |
          parseInt(result[2])
        ).toString(16).slice(1).toUpperCase()}`
      : null;
  };
  
  const applyGradientToHex = (hexColor) => {
    const elements = document.querySelectorAll('*');
    elements.forEach((element) => {
      const computedStyle = window.getComputedStyle(element);
      const backgroundColor = rgbToHex(computedStyle.backgroundColor);
  
      // Check if the background color matches the hexColor
      if (backgroundColor && backgroundColor.toLowerCase() === hexColor.toLowerCase()) {
        console.log('Element with matching color:', element);
        const gradientColors = ['#3024B5', '#4B3FD8', '#6558F9'];
        const gradient = `linear-gradient(180deg, ${gradientColors[0]}, ${gradientColors[1]}, ${gradientColors[2]})`;
        element.style.background = gradient;
        element.style.setProperty('background', gradient, 'important');
      }
    });
  };
  
  // Initial application of gradient
  applyGradientToHex('#695CFE');
  
  // Use MutationObserver to watch for DOM changes
  const observer = new MutationObserver(() => {
    applyGradientToHex('#695CFE');
  });
  
  // Configure the observer to watch for child additions and subtree changes
  observer.observe(document.body, {
    childList: true,
    subtree: true,
  });
  