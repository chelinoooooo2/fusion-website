(function() {
    // Reset the "recommendations" local storage
    localStorage.removeItem('recommendations');
    console.log('Local storage item "recommendations" has been reset.');
})();
