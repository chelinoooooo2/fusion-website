// Snowflake particle effect

// Create and configure the canvas
const canvas = document.createElement('canvas');
document.body.appendChild(canvas);
canvas.style.position = 'fixed'; // Use fixed so it stays in place during scrolling
canvas.style.top = '0';
canvas.style.left = '0';
canvas.style.width = '100%';
canvas.style.height = '100%';
canvas.style.pointerEvents = 'none'; // Prevent canvas from interfering with other elements
canvas.style.zIndex = '0'; // Ensure it's on top of most elements

const ctx = canvas.getContext('2d');

// Set canvas size to the full window
function resizeCanvas() {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
}
resizeCanvas();
window.addEventListener('resize', resizeCanvas);

// Snowflake class
class Snowflake {
    constructor() {
        this.reset();
    }

    reset() {
        this.x = Math.random() * canvas.width;
        this.y = Math.random() * canvas.height;
        this.radius = Math.random() * 4 + 1;
        this.speed = Math.random() * 1 + 0.5;
        this.wind = Math.random() * 2 - 1;
    }

    update() {
        this.y += this.speed;
        this.x += this.wind;

        // Reset snowflake if it goes out of bounds
        if (this.y > canvas.height || this.x < 0 || this.x > canvas.width) {
            this.reset();
            this.y = 0; // Start at the top
        }
    }

    draw() {
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
        ctx.fillStyle = 'rgba(200, 200, 255, 0.8)';
        ctx.fill();
    }
}

// Create and animate snowflakes
const snowflakes = [];
const numSnowflakes = 150;

for (let i = 0; i < numSnowflakes; i++) {
    snowflakes.push(new Snowflake());
}

let animationId;
let snowActive = true;

// Function to check and set the snow state based on local storage
function checkSnowState() {
    const storedState = localStorage.getItem('snowActive');
    if (storedState !== null) {
        snowActive = storedState === 'true';
    } else {
        snowActive = true; // Default state if nothing is stored
        localStorage.setItem('snowActive', snowActive); // Initialize local storage with default state
    }
}

function animate() {
    if (snowActive) {
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        snowflakes.forEach((flake) => {
            flake.update();
            flake.draw();
        });

        animationId = requestAnimationFrame(animate);
    }
}

// Function to toggle snow effect
function toggleSnow() {
    snowActive = !snowActive;
    localStorage.setItem('snowActive', snowActive);
    if (snowActive) {
        animate();
    } else {
        cancelAnimationFrame(animationId);
        ctx.clearRect(0, 0, canvas.width, canvas.height);
    }
}

// Add event listener for Ctrl + S to toggle snow effect
document.addEventListener('keydown', function(event) {
    if ((event.ctrlKey || event.metaKey) && event.key === 's') {
        event.preventDefault();  // Prevent any default behavior
        toggleSnow();
    }
});

// Initialize the snow state and animation
document.addEventListener('DOMContentLoaded', (event) => {
    checkSnowState();
    if (snowActive) {
        animate();
    }
});
