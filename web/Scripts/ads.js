  // Function to fade in the advertisement container
  function fadeInAdvertisement(advertisementId) {
    const advertisement = document.getElementById(advertisementId);
    advertisement.style.display = 'block';
    setTimeout(() => {
      advertisement.style.opacity = '1';
    }, 100);
  }

  // Check if the URL ends with # followed by any characters
  const url = window.location.href;
  if (/#.*$/.test(url)) {
    fadeInAdvertisement('rightAdvertisement');
    fadeInAdvertisement('bottomAdvertisement');
  }

  // Listen for hash changes and fade in advertisements if necessary
  window.addEventListener('hashchange', function() {
    if (/#.*$/.test(window.location.href)) {
      fadeInAdvertisement('rightAdvertisement');
      fadeInAdvertisement('bottomAdvertisement');
    }
  });