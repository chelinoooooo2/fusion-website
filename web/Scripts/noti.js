fetch('config.json')
  .then(response => response.json())
  .then(data => {
    const username = localStorage.getItem('sgs_profile_username');
    const message = data[username];
    if (message) {
      showNotification(username, message);
    }
  })
  .catch(error => console.error('Error loading notifications:', error));